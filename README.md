
## 1. Description

The ACME Land Model version 0 (ALMv0) is the starting version this model. 
The ALMv0 is same as the clm4_5_1_r085 tag, a development version of the 
Community Land Model (CLM). The model’s default functionality is scientifically
consistent with descriptions in Oleson et al. (2013). The following new 
developments have been incorporated within the code:

- A conceptual model to redistribute incoming snow accounting for microtopography.
- Development of PETSc-based three-dimensional models for simulating thermal
  and hydrologic processes.

The model has been applied to a polygonal tundra landscape at the Barrow 
Environmental Observatory in Alaska. A detailed description of the model and 
its application are available in Bisht et al. (2018).



## 2. Installation instructions


### 2.1. Compiler requirments

The code requires:

- C compiler
- Fortran compiler
- NetCDF
- PETSc

Set the following environment variables:

- `MPICC`   : Parallel C compiler
- `MPICXX`  : Parallel C++ compiler
- `MPIFC`   : Parallel Fortran compiler
- `MPIEXEC` : Path to MPI exec
- `SCC`     : Sequential C compiler
- `SCXX`    : Sequential C++ compiler
- `SFC`     : Sequential Fortran compiler


The code has been tested on Mac OS X 10.11.6 using GNU C and Fortran
compilers 5.4.0 installed via [MacPorts](https://www.macports.org/).
OpenMPI, NetCDF C, and NetCDF Fortran libraries were installed using
the GNU 5.4.0 compilers. An example of compiler related enviromnetal
variables are:

```
export MPICC=/opt/local/bin/mpicc-openmpi-gcc5
export MPICXX=/opt/local/bin/mpicxx-openmpi-gcc5
export MPIFC=/opt/local/bin/mpif90-openmpi-gcc5
export MPIEXEC=/opt/local/bin/mpiexec-openmpi-gcc5
export SCC=/opt/local/bin/gcc-mp-5
export SCXX=/opt/local/bin/g++-mp-5
export SFC=/opt/local/bin/gfortran-mp-5
```


### 2.2. Install PETSc

The code requires [PETSc](http://www.mcs.anl.gov/petsc/) and the current version 
of the model has been tested with a particular version 
([c41c766](https://bitbucket.org/petsc/petsc/src/c41c766)) of PETSc.

#### Clone PETSc

```
cd <directory-of-choice>
export PETSC_HASH=c41c766
git clone https://bitbucket.org/petsc/petsc petsc_$PETSC_HASH
cd petsc_$PETSC_HASH
git checkout $PETSC_HASH
```

#### Build and test PETSc

```
export PETSC_DIR=$PWD

./configure \
--with-cc=$MPICC --with-cxx=$MPICXX --with-fc=$MPIFC \
--with-mpiexec=$MPIEXEC --download-sowing=1 PETSC_ARCH=$PETSC_ARCH

make PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH all

make PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH test
```


### 2.3. Download data

#### Data for the NGEE-Arctic study

```
cd <directory-of-choice>
git clone https://bitbucket.org/gbisht/notes-for-gmd-2017-71/
export DATA_REPO_DIR=$PWD/notes-for-gmd-2017-71
export DIN_LOC_ROOT=$DATA_REPO_DIR/data
```

#### Default data from CESM

```
cd $DIN_LOC_ROOT
mkdir -p atm/cam/chem/trop_mozart/emis
mkdir -p atm/cam/chem/trop_mozart_aero/aero
mkdir -p lnd/clm2/lai_streams
mkdir -p lnd/clm2/paramdata
mkdir -p lnd/clm2/snicardata

NCAR_INPUTDATA_SERVER=https://svn-ccsm-inputdata.cgd.ucar.edu/trunk/inputdata

svn export $NCAR_INPUTDATA_SERVER/atm/cam/chem/trop_mozart/emis/megan21_emis_factors_c20120313.nc \
atm/cam/chem/trop_mozart/emis/megan21_emis_factors_c20120313.nc

svn export $NCAR_INPUTDATA_SERVER/atm/cam/chem/trop_mozart_aero/aero/aerosoldep_monthly_1850_mean_1.9x2.5_c090421.nc \
atm/cam/chem/trop_mozart_aero/aero/aerosoldep_monthly_1850_mean_1.9x2.5_c090421.nc

svn export $NCAR_INPUTDATA_SERVER/lnd/clm2/lai_streams/MODISPFTLAI_0.5x0.5_c140711.nc \
lnd/clm2/lai_streams/MODISPFTLAI_0.5x0.5_c140711.nc 

svn export $NCAR_INPUTDATA_SERVER/lnd/clm2/paramdata/clm_params.c140423.nc \
lnd/clm2/paramdata/clm_params.c140423.nc 

svn export $NCAR_INPUTDATA_SERVER/lnd/clm2/snicardata/snicar_drdt_bst_fit_60_c070416.nc \
lnd/clm2/snicardata/snicar_drdt_bst_fit_60_c070416.nc

svn export $NCAR_INPUTDATA_SERVER/lnd/clm2/snicardata/snicar_optics_5bnd_c090915.nc \
lnd/clm2/snicardata/snicar_optics_5bnd_c090915.nc

```


### 2.4. Download the code

```
cd <directory-of-choice>
git clone https://bitbucket.org/gbisht/lateral-subsurface-model/
SRC_DIR=$PWD/lateral-subsurface-model
```


### 2.5. Build a case

Set environment variables `CASE_DIR` and `CASE_NAME`

```
export CASE_DIR=<directory-of-choice>
export CASE_NAME=A_transect_416x1pt_I1850CLM45_US-Brw
```

An example to configure, build and run a simulation with following settings:

- Snow redstribution is turned on (`-snow_redistribution true`)
- Uses PETSc based thermal model (`-petsc_thermal_model true`) with 
  lateral subsurface redistribution of heat (`-thermal_redistribution true`)
- Uses PETSc based hydrology model (`-petsc_unsaturated_flow_model true`) with 
  lateral subsurface redistribution of water (`-unsaturated_flow_redistribution true`)

```
cd $DATA_REPO_DIR
./create_case_for_NGEE_Site_US-Brw.sh \
-source_dir                      $SRC_DIR \
-case_dir                        $CASE_DIR \
-inputdata_dir                   $DIN_LOC_ROOT \
-snow_redistribution             true \
-petsc_thermal_model             true \
-thermal_redistribution          true \
-petsc_unsaturated_flow_model    true \
-unsaturated_flow_redistribution true \
-case_name                       $CASE_NAME
```


## References

Bisht, G., Riley, W. J., Wainwright, H. M., Dafflon, B., Yuan, F., and Romanovsky, V. E.:
Impacts of microtopographic snow redistribution and lateral subsurface processes on
hydrologic and thermal states in an Arctic polygonal ground ecosystem: a case study using
ELM-3D v1.0, Geosci. Model Dev., 11, 61-76, https://doi.org/10.5194/gmd-11-61-2018, 2018.

Oleson, K. W., D.M. Lawrence, G.B. Bonan, B. Drewniak, M. Huang, C.D. Koven, S. Levis,
F. Li, W.J. Riley, Z.M. Subin, S.C. Swenson, P.E. Thornton, A. Bozbiyik, R. Fisher, 
E. Kluzek, J.-F. Lamarque, P.J. Lawrence, L.R. Leung, W. Lipscomb, S. Muszala, 
D.M. Ricciuto, W. Sacks, Y. Sun, J. Tang, Z.-L. Yang: Technical Description of version 4.5
of the Community Land Model (CLM), National Center for Atmospheric Research, Boulder, CO, 422 pp., 2013
