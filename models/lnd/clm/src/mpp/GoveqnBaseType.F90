!> @file
!> @brief Base class for representing governing equations.


#ifdef USE_PETSC_LIB


!> @brief Base class for representing governing equations.
!!
!! Provides some concrete methods, and defines error-throwing placeholders for
!! routines that derived classes must implement.
!!
module GoverningEquationBaseType

  !-----------------------------------------------------------------------
  ! !DESCRIPTION:
  ! Govneqn data type allocation
  !-----------------------------------------------------------------------

  use MeshType
  use ConditionType
  !use FieldBaseType
  !
  ! !PUBLIC TYPES:
  implicit none
  private

#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscvec.h90"
#include "finclude/petscmat.h"
#include "finclude/petscmat.h90"

  type, public :: goveqn_base_type
     !
     character (len=256)                      :: name
     PetscInt                                 :: id
     PetscInt                                 :: id_in_list
     class(mesh_type),pointer                 :: mesh
     PetscInt                                 :: mesh_itype
     type(condition_list_type),pointer        :: boundary_conditions
     type(condition_list_type),pointer        :: source_sinks

     PetscReal                                :: dtime   ! [sec]

     ! Track variables supplied by other governing equations.
     PetscInt                                 :: nvars_needed_from_other_goveqns
     PetscInt, pointer                        :: var_ids_needed_from_other_goveqns(:)
     PetscInt, pointer                        :: ids_of_other_goveqns(:)
     PetscBool, pointer                       :: is_bc_auxvar_type(:)
     PetscInt, pointer                        :: bc_auxvar_offset(:)
     PetscInt, pointer                        :: bc_auxvar_idx(:)
     PetscInt, pointer                        :: bc_auxvar_idx_of_other_goveqn(:)
     PetscInt, pointer                        :: bc_auxvar_ncells(:)

     class(goveqn_base_type),pointer          :: next
  contains
     procedure, public :: Init                  => GoveqnBaseInit
     procedure, public :: Clean                 => GoveqnBaseClean
     procedure, public :: AllocVarsFromOtherGEs => GoveqnBase_AllocVarsFromOtherGEs
     procedure, public :: DeallocVarsFromOtherGEs => GoveqnBase_DeallocVarsFromOtherGEs
     procedure, public :: PrintInfo             => GoveqnBasePrintInfo
     procedure, public :: UpdateAuxVars         => GoveqnBaseUpdateAuxVars
     procedure, public :: UpdateAuxVarsIntrn    => GoveqnBaseUpdateAuxVarsIntrn
     procedure, public :: UpdateAuxVarsBC       => GoveqnBaseUpdateAuxVarsBC
     procedure, public :: UpdateAuxVarsSS       => GoveqnBaseUpdateAuxVarsSS
     procedure, public :: PreSolve              => GoveqnBasePreSolve
     procedure, public :: IFunction             => GoveqnBaseIFunction
     procedure, public :: IJacobian             => GoveqnBaseIJacobian
     procedure, public :: IJacobianOffDiag      => GoveqnBaseIJacobianOffDiag
     procedure, public :: JacobianOffDiag       => GoveqnBaseJacobianOffDiag
     procedure, public :: Jacobian              => GoveqnBaseJacobian
     procedure, public :: Residual              => GoveqnBaseResidual
     procedure, public :: ComputeRHS            => GoveqnBaseComputeRHS
     procedure, public :: ComputeOperators      => GoveqnBaseComputeOperators
     procedure, public :: SetDtime              => GoveqnBaseSetDtime
  end type goveqn_base_type
  !------------------------------------------------------------------------

  public :: GoveqnBasePrintInfo

contains

  !------------------------------------------------------------------------
  subroutine GoveqnBaseInit(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type) :: this

    this%name   = ""
    this%id     = -1
    this%id_in_list = -1

    this%dtime = 0.d0

    this%nvars_needed_from_other_goveqns = 0
    nullify(this%var_ids_needed_from_other_goveqns)
    nullify(this%ids_of_other_goveqns)
    nullify(this%is_bc_auxvar_type)
    nullify(this%bc_auxvar_offset)
    nullify(this%bc_auxvar_idx)
    nullify(this%bc_auxvar_idx_of_other_goveqn)
    nullify(this%bc_auxvar_ncells)

    nullify(this%mesh)
    nullify(this%boundary_conditions)
    nullify(this%source_sinks)
    !this%field => FieldInit()
    nullify(this%next)

  end subroutine GoveqnBaseInit

  !------------------------------------------------------------------------
  subroutine GoveqnBaseClean(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type) :: this

    call ConditionListClean(this%boundary_conditions)
    call ConditionListClean(this%source_sinks)

  end subroutine GoveqnBaseClean


  !------------------------------------------------------------------------
  !> @brief Allocate memory for tracking variables provided by other governing equations.
  !!
  !! @param[in] nvars Count of variables.
  !!
  subroutine GoveqnBase_AllocVarsFromOtherGEs(this, nvars)
      implicit none

      ! !ARGUMENTS
      class(goveqn_base_type), intent(inout):: this
      PetscInt, intent(in):: nvars

      ! !LOCAL VARIABLES:
      integer:: istat

      if( this%nvars_needed_from_other_goveqns /= 0 ) then
          stop 'GoveqnBase_AllocVarsFromOtherGEs: Bad initialization'
      endif

      this%nvars_needed_from_other_goveqns = nvars
      allocate(  &
        this%var_ids_needed_from_other_goveqns(nvars),  &
        this%ids_of_other_goveqns(nvars),  &
        this%is_bc_auxvar_type(nvars),  &
        this%bc_auxvar_offset(nvars),  &
        this%bc_auxvar_idx(nvars), &
        this%bc_auxvar_idx_of_other_goveqn(nvars), &
        this%bc_auxvar_ncells(nvars), &
        STAT=istat  &
        )
      if( istat /= 0 ) then
          stop 'GoveqnBase_AllocVarsFromOtherGEs: Bad alloc'
      endif

  end subroutine GoveqnBase_AllocVarsFromOtherGEs


  !------------------------------------------------------------------------
  !> @brief De-allocate memory for tracking variables provided by other governing equations.
  !!
  !! Undo sbr `GoveqnBase_AllocVarsFromOtherGEs()`.
  !!
  subroutine GoveqnBase_DeallocVarsFromOtherGEs(this)
      implicit none

      ! !ARGUMENTS
      class(goveqn_base_type), intent(inout):: this

      if( this%nvars_needed_from_other_goveqns <= 0 ) then
          stop 'GoveqnBase_AllocVarsFromOtherGEs: Not allocated'
      endif

      this%nvars_needed_from_other_goveqns = 0
      deallocate(  &
        this%var_ids_needed_from_other_goveqns,  &
        this%ids_of_other_goveqns,  &
        this%is_bc_auxvar_type,  &
        this%bc_auxvar_offset,  &
        this%bc_auxvar_idx, &
        this%bc_auxvar_idx_of_other_goveqn, &
        this%bc_auxvar_ncells, &
        )

  end subroutine GoveqnBase_DeallocVarsFromOtherGEs


  !------------------------------------------------------------------------
  subroutine GoveqnBaseIFunction(this, U, Udot, F, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type)       :: this
    Vec                           :: U
    Vec                           :: Udot
    Vec                           :: F
    PetscErrorCode                :: ierr

    write(*,*)'GoveqnBaseIFunction must be extended by child class.'
    stop

  end subroutine GoveqnBaseIFunction

  !------------------------------------------------------------------------
  subroutine GoveqnBaseIJacobian(this, U, Udot, shift, A, B, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type)       :: this
    Vec                           :: U
    Vec                           :: Udot
    PetscReal                     :: shift
    Mat                           :: A
    Mat                           :: B
    PetscErrorCode                :: ierr

    write(*,*)'GoveqnBaseJFunction must be extended by child class.'
    stop

  end subroutine GoveqnBaseIJacobian

  !------------------------------------------------------------------------
  subroutine GoveqnBaseIJacobianOffDiag(this, U_1, Udot_1, U_2, Udot_2, &
                                        shift, A, B, id_of_other_goveq, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type)       :: this
    Vec                           :: U_1
    Vec                           :: Udot_1
    Vec                           :: U_2
    Vec                           :: Udot_2
    PetscReal                     :: shift
    Mat                           :: A
    Mat                           :: B
    PetscInt                      :: id_of_other_goveq
    PetscErrorCode                :: ierr

    write(*,*)'GoveqnBaseIJacobianOffDiag must be extended by child class.'
    stop

  end subroutine GoveqnBaseIJacobianOffDiag

  !------------------------------------------------------------------------
  subroutine GoveqnBaseJacobianOffDiag(this, X_1, X_2, A, B, &
                                       id_of_other_goveq, &
                                       list_id_of_other_goveq, &
                                       ierr)

    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type)       :: this
    Vec                           :: X_1
    Vec                           :: X_2
    Mat                           :: A
    Mat                           :: B
    PetscInt                      :: id_of_other_goveq
    PetscInt                      :: list_id_of_other_goveq
    PetscErrorCode                :: ierr

    write(*,*)'GoveqnBaseJacobianOffDiag must be extended by child class.'
    stop

  end subroutine GoveqnBaseJacobianOffDiag

  !------------------------------------------------------------------------
  subroutine GoveqnBaseResidual(this, X, F, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type)       :: this
    Vec                           :: X
    Vec                           :: F
    PetscErrorCode                :: ierr

    write(*,*)'GoveqnBaseResidual must be extended by child class.'
    stop

  end subroutine GoveqnBaseResidual

  !------------------------------------------------------------------------
  subroutine GoveqnBaseComputeRHS(this, R, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type)       :: this
    Vec                           :: R
    PetscErrorCode                :: ierr

    write(*,*)'GoveqnBaseComputeRHS must be extended by child class.'
    stop

  end subroutine GoveqnBaseComputeRHS

  !------------------------------------------------------------------------
  subroutine GoveqnBaseComputeOperators(this, A, B, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type)       :: this
    Mat                           :: A
    Mat                           :: B
    PetscErrorCode                :: ierr

    write(*,*)'GoveqnBaseComputeOperators must be extended by child class.'
    stop

  end subroutine GoveqnBaseComputeOperators

  !------------------------------------------------------------------------
  subroutine GoveqnBaseSetDtime(this, dtime)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type)       :: this
    PetscReal                     :: dtime

    this%dtime = dtime

  end subroutine GoveqnBaseSetDtime


  !------------------------------------------------------------------------
  subroutine GoveqnBaseJacobian(this, X, A, B, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type)       :: this
    Vec                           :: X
    Mat                           :: A
    Mat                           :: B
    PetscErrorCode                :: ierr

    write(*,*)'GoveqnBaseJacobian must be extended by child class.'
    stop

  end subroutine GoveqnBaseJacobian

  !------------------------------------------------------------------------
  subroutine GoveqnBasePrintInfo(this)
    !
    ! !DESCRIPTION:
    !
    use ConditionType, only : ConditionListPrintInfo
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type) :: this

    write(*,*)'    ---------------------------------------------------------'
    write(*,*)'    Goveqn_name       : ',trim(this%name)
    write(*,*)'    Goveqn_id         : ',this%id
    write(*,*)'    Goveqn_mesh_itype : ',this%mesh_itype
    write(*,*)'    Num_vars_needed   : ',this%nvars_needed_from_other_goveqns
    write(*,*)' '

    write(*,*)'    BC'
    call ConditionListPrintInfo(this%boundary_conditions)
    write(*,*)'    SS'
    call ConditionListPrintInfo(this%source_sinks)

  end subroutine GoveqnBasePrintInfo

  !------------------------------------------------------------------------
  subroutine GoveqnBaseUpdateAuxVars(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type) :: this

    write(*,*) 'In GoveqnBaseUpdateAuxVars: This needs to be extended by ' // &
      'child class.'
    stop

  end subroutine GoveqnBaseUpdateAuxVars

  !------------------------------------------------------------------------
  subroutine GoveqnBaseUpdateAuxVarsIntrn(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type) :: this

    write(*,*) 'In GoveqnBaseUpdateAuxVarsIntrn: This needs to be extended by ' // &
      'child class.'
    stop

  end subroutine GoveqnBaseUpdateAuxVarsIntrn

  !------------------------------------------------------------------------
  subroutine GoveqnBaseUpdateAuxVarsBC(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type) :: this

    write(*,*) 'In GoveqnBaseUpdateAuxVarsBC: This needs to be extended by ' // &
      'child class.'
    stop

  end subroutine GoveqnBaseUpdateAuxVarsBC

  !------------------------------------------------------------------------
  subroutine GoveqnBaseUpdateAuxVarsSS(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type) :: this

    write(*,*) 'In GoveqnBaseUpdateAuxVarsSS: This needs to be extended by ' // &
      'child class.'
    stop

  end subroutine GoveqnBaseUpdateAuxVarsSS

  !------------------------------------------------------------------------
  subroutine GoveqnBasePreSolve(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(goveqn_base_type) :: this

    write(*,*) 'In GoveqnBasePreSolve: This needs to be extended by ' // &
      'child class.'
    stop

  end subroutine GoveqnBasePreSolve

end module GoverningEquationBaseType
#endif