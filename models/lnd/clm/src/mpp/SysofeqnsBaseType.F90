#ifdef USE_PETSC_LIB
module SystemOfEquationsBaseType

  !-----------------------------------------------------------------------
  ! !DESCRIPTION:
  ! Base object for a system-of-equations
  !-----------------------------------------------------------------------

  use MeshType
  use GoverningEquationBaseType
  use abortutils       , only : endrun
  
  implicit none
  private

#include "finclude/petscsys.h"
#include "finclude/petscvec.h"
#include "finclude/petscvec.h90"
#include "finclude/petscmat.h"
#include "finclude/petscmat.h90"
#include "finclude/petscts.h"
#include "finclude/petscts.h90"
#include "finclude/petscsnes.h"
#include "finclude/petscsnes.h90"
#include "finclude/petscdm.h"
#include "finclude/petscdm.h90"
#include "finclude/petscdmda.h"
#include "finclude/petscdmda.h90"
#include "finclude/petscviewer.h"
#include "finclude/petscksp.h"
#include "finclude/petscksp.h90"

  type, public :: sysofeqns_base_type
    character(len =256)                  :: name
    PetscInt                             :: itype

    class(goveqn_base_type),pointer      :: goveqns
    PetscInt                             :: ngoveqns


    !
    PetscReal :: time  ! [sec]
    PetscReal :: dtime ! [sec]

    ! GB: Make solver-type
    PetscInt :: solver_type
    DM       :: dm
    TS       :: ts
    SNES     :: snes
    KSP      :: ksp

    ! GB: Make a field-type
    Vec :: soln
    Vec :: soln_prev
    Vec :: rhs
    Mat :: jac
    Mat :: Amat

  contains
    procedure, public :: Init                   => SOEBaseInit
    procedure, public :: Clean                  => SOEBaseClean
    procedure, public :: IFunction              => SOEBaseIFunction
    procedure, public :: IJacobian              => SOEBaseIJacobian
    procedure, public :: Residual               => SOEBaseResidual
    procedure, public :: Jacobian               => SOEBaseJacobian
    procedure, public :: ComputeRHS             => SOEComputeRHS
    procedure, public :: ComputeOperators       => SOEComputeOperators
    procedure, public :: StepDT                 => SOEBaseStepDT
    procedure, public :: PreSolve               => SOEBasePreSolve
    procedure, public :: PostSolve              => SOEBasePostSolve
    procedure, public :: PrintInfo              => SOEBasePrintInfo
    procedure, public :: SetPointerToIthGovEqn  => SOEBaseSetPointerToIthGovEqn
    procedure, public :: SetDtime               => SOEBaseDtime
    procedure, public :: SetDataFromCLM         => SOEBaseSetDataFromCLM
    procedure, public :: GetDataFromCLM         => SOEBaseGetDataFromCLM
  end type

  public :: SOEBaseInit
  public :: SOESetMeshesOfGoveqns

  !------------------------------------------------------------------------
contains

  !------------------------------------------------------------------------
  subroutine SOEBaseInit(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type) :: this

    this%name      = ""
    this%itype     = 0
    this%ngoveqns  = 0

    this%time  = 0.d0
    this%dtime = 0.d0

    this%solver_type = 0
    this%dm          = 0
    this%ts          = 0
    this%snes        = 0
    this%ksp         = 0

    this%soln        = 0
    this%soln_prev   = 0
    this%rhs         = 0
    this%jac         = 0
    this%Amat        = 0

    nullify(this%goveqns)

  end subroutine SOEBaseInit

  !------------------------------------------------------------------------
  subroutine SOEBaseIFunction(this, ts, t, U, Udot, F, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type)    :: this
    TS                            :: ts
    PetscReal                     :: t
    Vec                           :: U
    Vec                           :: Udot
    Vec                           :: F
    PetscErrorCode                :: ierr

    call endrun(msg='ERROR SystemOfEquationsBaseType: '//&
         'SOEBaseIFunction must be extended')

  end subroutine SOEBaseIFunction

  !------------------------------------------------------------------------
  subroutine SOEBaseIJacobian(this, ts, t, U, Udot, shift, A, B, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type)    :: this
    TS                            :: ts
    PetscReal                     :: t
    Vec                           :: U
    Vec                           :: Udot
    PetscReal                     :: shift
    Mat                           :: A
    Mat                           :: B
    PetscErrorCode                :: ierr

    call endrun(msg='ERROR SystemOfEquationsBaseType: '//&
         'SOEBaseIJacobian must be extended')

  end subroutine SOEBaseIJacobian

  !------------------------------------------------------------------------
  subroutine SOEBaseResidual(this,snes, X, F, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type)    :: this
    SNES                          :: snes
    PetscReal                     :: t
    Vec                           :: X
    Vec                           :: F
    PetscErrorCode                :: ierr

    call endrun(msg='ERROR SystemOfEquationsBaseType: '//&
         'SOEBaseResidual must be extended')

  end subroutine SOEBaseResidual

  !------------------------------------------------------------------------
  subroutine SOEBaseJacobian(this, snes, X, A, B, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type)    :: this
    SNES                          :: snes
    Vec                           :: X
    Mat                           :: A
    Mat                           :: B
    PetscErrorCode                :: ierr

    call endrun(msg='ERROR SystemOfEquationsBaseType: '//&
         'SOEBaseJacobian must be extended')

  end subroutine SOEBaseJacobian

  !------------------------------------------------------------------------
  subroutine SOEComputeRHS(this, ksp, B, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type)    :: this
    KSP                           :: ksp
    Mat                           :: B
    PetscErrorCode                :: ierr

    call endrun(msg='ERROR SOEComputeRHS: '//&
         'SOEComputeRHS must be extended')

  end subroutine SOEComputeRHS

  !------------------------------------------------------------------------
  subroutine SOEComputeOperators(this, ksp, A, B, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type)    :: this
    KSP                           :: ksp
    Mat                           :: A
    Mat                           :: B
    PetscErrorCode                :: ierr

    call endrun(msg='ERROR SOEComputeOperators: '//&
         'SOEComputeOperators must be extended')

  end subroutine SOEComputeOperators

  !------------------------------------------------------------------------
  subroutine SOEBasePreSolve(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type)    :: this

    call endrun(msg='ERROR SystemOfEquationsBaseType: '//&
         'SOEBasePreSolve must be extended')

  end subroutine SOEBasePreSolve

  !------------------------------------------------------------------------
  subroutine SOEBaseStepDT(this, dt, ierr)
    !
    ! !DESCRIPTION:
    !
    ! !USES
    use MultiPhysicsProbConstants, only : PETSC_TS
    use MultiPhysicsProbConstants, only : PETSC_SNES
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type)    :: this
    PetscReal                     :: dt
    PetscErrorCode                :: ierr

    select case(this%solver_type)
    case (PETSC_TS)
      call SOEBaseStepDT_TS(this, dt, ierr)
    case (PETSC_SNES)
      call SOEBaseStepDT_SNES(this, dt, ierr)
    case default
       write(*,*) 'VSFMMPPSetup: Unknown this%solver_type'
       stop
    end select

  end subroutine SOEBaseStepDT

  !------------------------------------------------------------------------
  subroutine SOEBaseStepDT_TS(soe, dt, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type)    :: soe
    PetscReal                     :: dt
    PetscErrorCode                :: ierr
    !
    ! !LOCAL VARIABLES:
    SNESConvergedReason           :: snes_reason

    call TSSetTime(soe%ts, 0.d0, ierr); CHKERRQ(ierr)
    call TSSetDuration(soe%ts, 100000, dt, ierr); CHKERRQ(ierr)
    call TSsetInitialTimeStep(soe%ts, 0.0d0, 3600.0d0, ierr); CHKERRQ(ierr)

    call TSSetFromOptions(soe%ts, ierr); CHKERRQ(ierr)

    call TSSolve(soe%ts, soe%soln, ierr); CHKERRQ(ierr)

  end subroutine SOEBaseStepDT_TS

  !------------------------------------------------------------------------
  subroutine SOEBaseStepDT_SNES(soe, dt, ierr)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type)    :: soe
    PetscErrorCode                :: ierr
    PetscReal                     :: dt
    !
    ! !LOCAL VARIABLES:
    SNESConvergedReason           :: snes_reason

    soe%time = 0.d0
    call soe%SetDtime(dt)

    call soe%PreSolve()

    call SNESSolve(soe%snes, PETSC_NULL_OBJECT, soe%soln, ierr); CHKERRQ(ierr)

    call SNESGetConvergedReason(soe%snes, snes_reason, ierr); CHKERRQ(ierr)

    if (snes_reason < 0) then
      write(*,*)'In SOEBaseStepDT_SNES: snes_reason diverged add code.'
      stop
    endif

    call soe%PostSolve()

  end subroutine SOEBaseStepDT_SNES

  !------------------------------------------------------------------------
  subroutine SOEBasePostSolve(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type) :: this
    !
    ! !LOCAL VARIABLES:
    PetscErrorCode             :: ierr

    call VecCopy(this%soln, this%soln_prev,ierr); CHKERRQ(ierr)

  end subroutine SOEBasePostSolve

  !------------------------------------------------------------------------
  subroutine SOEBasePrintInfo(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type) :: this
    !
    ! !LOCAL VARIABLES:
    class(goveqn_base_type),pointer :: cur_goveqn

    write(*,*)'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
    write(*,*)'  SystemOfEqns_name  : ',trim(this%name)
    write(*,*)'  SystemOfEqns_itype : ',this%itype
    write(*,*)''

    cur_goveqn => this%goveqns
    do
       if (.not.associated(cur_goveqn)) exit
       call cur_goveqn%PrintInfo()
       cur_goveqn => cur_goveqn%next
    enddo
    write(*,*)'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'

  end subroutine SOEBasePrintInfo

  !------------------------------------------------------------------------
  subroutine SOESetMeshesOfGoveqns(soe, meshes, nmesh)
    !
    ! !DESCRIPTION:
    !
    ! !USES
    use MeshType
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type) :: soe
    class(mesh_type), pointer  :: meshes(:)
    PetscInt                   :: nmesh
    !
    ! !LOCAL VARIABLES:
    PetscInt                           :: imesh
    PetscInt                           :: mesh_itype
    PetscBool                          :: mesh_found
    class(mesh_type), pointer          :: cur_mesh
    class(goveqn_base_type),pointer    :: cur_goveqn

    cur_goveqn => soe%goveqns
    do
       if (.not.associated(cur_goveqn)) exit
       mesh_itype = cur_goveqn%mesh_itype

       mesh_found = PETSC_FALSE
       do imesh = 1, nmesh
          cur_mesh => meshes(imesh)
          if (mesh_itype == cur_mesh%itype) then
             cur_goveqn%mesh => cur_mesh
             mesh_found = PETSC_TRUE
             exit
          endif
       enddo

       if (.not.mesh_found) then
          call endrun(msg='ERROR SystemOfEquationsBaseType: '//&
               'Mesh not found for Governing equation within the list')
       endif

      cur_goveqn => cur_goveqn%next
    enddo

  end subroutine SOESetMeshesOfGoveqns

  !------------------------------------------------------------------------
  subroutine SOEBaseSetPointerToIthGovEqn(this, goveqn_id, goveqn_ptr)
    !
    ! !DESCRIPTION:
    !
    ! !USES
    use MeshType
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type) :: this
    PetscInt :: goveqn_id
    class(goveqn_base_type),pointer      :: goveqn_ptr
    !
    ! !LOCAL VARIABLES
    class(goveqn_base_type),pointer :: cur_goveq
    PetscInt :: sum_goveqn
    PetscBool :: found

    sum_goveqn = 0
    found = PETSC_FALSE
    cur_goveq => this%goveqns
    do
       if (.not.associated(cur_goveq)) exit
       sum_goveqn = sum_goveqn + 1
       if (sum_goveqn == goveqn_id) then
          found = PETSC_TRUE
          goveqn_ptr => cur_goveq
          exit
       endif

       cur_goveq => cur_goveq%next
    enddo

    if (.not.found) then
       write(*,*) 'In SOEBaseSetPointerToIthGovEqn: ith-goveqn not found'
       stop
    endif

  end subroutine SOEBaseSetPointerToIthGovEqn

  !------------------------------------------------------------------------
  subroutine SOEBaseDtime(this, dtime)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type) :: this
    PetscReal                  :: dtime
    !
    ! !LOCAL VARIABLES:
    class(goveqn_base_type),pointer :: cur_goveqn

    this%dtime = dtime

    cur_goveqn => this%goveqns
    do
       if (.not.associated(cur_goveqn)) exit
       call cur_goveqn%SetDtime(dtime)
       cur_goveqn => cur_goveqn%next
    enddo

  end subroutine SOEBaseDtime

  !------------------------------------------------------------------------
  subroutine SOEBaseSetDataFromCLM(this, soe_auxvar_type, var_type, soe_auxvar_id, data_1d)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(sysofeqns_base_type) :: this
    PetscInt :: var_type
    PetscInt :: soe_auxvar_type
    PetscInt :: soe_auxvar_id
    PetscReal :: data_1d(:)
    !
    ! !LOCAL VARIABLES:

    call endrun(msg='ERROR SystemOfEquationsBaseType: '//&
         'SOEBaseSetDataFromCLM must be extended')

  end subroutine SOEBaseSetDataFromCLM

  !------------------------------------------------------------------------
  subroutine SOEBaseGetDataFromCLM(this, soe_auxvar_type, var_type, soe_auxvar_id, data_1d)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS
    class(sysofeqns_base_type) :: this
    PetscInt :: var_type
    PetscInt :: soe_auxvar_type
    PetscInt :: soe_auxvar_id
    PetscReal :: data_1d(:)
    PetscInt :: nsize
    !
    ! !LOCAL VARIABLES:

    call endrun(msg='ERROR SystemOfEquationsBaseType: '//&
         'SOEBaseGetDataFromCLM must be extended')

  end subroutine SOEBaseGetDataFromCLM

  !------------------------------------------------------------------------
  subroutine SOEBaseClean(this)
    !
    ! !DESCRIPTION:
    !
    implicit none
    !
    ! !ARGUMENTS    
    class(sysofeqns_base_type) :: this

    !call this%goveqns%Clean

    nullify(this%goveqns)

  end subroutine SOEBaseClean

end module SystemOfEquationsBaseType

#endif